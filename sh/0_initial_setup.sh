#!/bin/bash
#change directory to current script
cd "${0%/*}"


#Check for input
if [ "$#" -ne 1 ]; then
  echo "usage ./system_setup.sh <node_type>"
  exit -1
fi

#Turn input to caps
NODE_TYPE=${1^^}

#Used if instead of switch for readability
if [ "$NODE_TYPE" = "ROUTER" ]; then
  echo "NODE_TYPE=ROUTER"
  cp "../conf/router/dhcpcd.conf" "/etc/dhcpcd.conf"
elif [ "$NODE_TYPE" = "TAIL" ]; then
  echo "NODE_TYPE=TAIL"
elif [ "$NODE_TYPE" = "LERRING" ]; then
 echo "NODE_TYPE=LERRING"
elif [ "$NODE_TYPE" = "TEST" ]; then
 echo "NODE_TYPE=TEST"
else
 echo "<node_type> must be lerring, router, tail, or test"
fi

#Production - Gen Keys for each system
#Project - key pre generated and stored in git for scope
  #openssl genrsa -out /lerring/trusted_keys/client.key 4096
  #openssl req -new -x509 -days 365 -key /lerring/trusted_keys/client.key -out /lerring/trusted_keys/client.crt

#Copy conf file and modify based on NODE_TYPE
rm -rf /lerring && mkdir "/lerring"
cp ../conf/lerring/* /lerring -r

sed -i "s/\(node\.type=\).*\$/\1${NODE_TYPE}/" "/lerring/lerring.conf"
UUID=$(cat /proc/sys/kernel/random/uuid)
sed -i "s/\(node\.uuid=\).*\$/\1${UUID}/" "/lerring/lerring.conf"

#ToDo: update networking without needing reboot
#sudo reboot
