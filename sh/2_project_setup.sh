#!/bin/bash
# cd to this scripts directory
  cd "${0%/*}"
  cd ../..

if [ "$#" -ne 1 ]; then
  echo "usage ./system_setup.sh <node_type>"
  exit -1
fi

#setup git credentials
git config --global user.email lee.herring97@gmail.com
git config --global user.name lerring
#clone repos
git clone git@gitlab.com:lee_dissertation/phase-two/base.git
git clone git@gitlab.com:lee_dissertation/phase-two/lerring_framework.git
#build images
docker-compose -f base/docker-compose.yml build
docker-compose -f lerring_framework/docker-compose.yml build

NODE_TYPE=${1^^}
if [ "$NODE_TYPE" = "ROUTER" ]; then

  echo "NODE_TYPE=ROUTER"
  git clone git@gitlab.com:lee_dissertation/phase-two/router_framework.git
  docker-compose -f router_framework/docker-compose.yml build

elif [ "$NODE_TYPE" = "TAIL" ]; then

  echo "NODE_TYPE=TAIL"
  git clone git@gitlab.com:lee_dissertation/phase-two/tail_framework.git
  docker-compose -f tail_framework/docker-compose.yml build

elif [ "$NODE_TYPE" = "LERRING" ]; then

  echo "NODE_TYPE=LERRING"
  #place worker apps

elif [ "$NODE_TYPE" = "TEST" ]; then

  echo "NODE_TYPE=TEST"
  #place worker apps
  git clone git@gitlab.com:lee_dissertation/phase-two/test_runner.git
  docker-compose -f test_runner/docker-compose.yml build

else

 echo "<node_type> must be lerring, router, tail, or test"

fi

