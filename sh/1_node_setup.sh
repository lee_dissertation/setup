#!/bin/bash
#change directory to current script
cd "${0%/*}"

if [ "$#" -ne 1 ]; then
  echo "usage ./system_setup.sh <node_type>"
  exit -1
fi

docker network create -d bridge br0

NODE_TYPE=${1^^}
if [ "$NODE_TYPE" = "ROUTER" ]; then
  echo "NODE_TYPE=ROUTER"
  #forwarding
  iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
  iptables -A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
  iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT
  apt install iptables-persistent
elif [ "$NODE_TYPE" = "TAIL" ]; then
  echo "NODE_TYPE=TAIL"
  #nothing needed yes
elif [ "$NODE_TYPE" = "LERRING" ]; then
  echo "NODE_TYPE=LERRING"
  #nothing needed yet
elif [ "$NODE_TYPE" = "TEST" ]; then
  echo "NODE_TYPE=TEST"
  #nothing needed yet
else
 echo "<node_type> must be lerring, router, or tail"
fi

