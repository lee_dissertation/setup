# Setup
This project will be used to manually setup and install the components needed to build the lerring service,
this will include installing docker + docker-compose, preparing the node type, and downloading core git repositories
## Pre-requisites
Create a new user and login
```
sudo adduser lerring
sudo usermod lerring -aG adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,spi,i2c,gpio
su - lerring
```
Install docker and docker compose, setting up required networks
```
#install docker
sudo curl -sSL https://get.docker.com | sh
sudo usermod -aG docker lerring
#install docker-compose
sudo apt-get install python3-pip python3-dev -y
sudo pip3 install --upgrade setuptools
sudo apt-get install libffi-dev
sudo pip3 install docker-compose
sudo newgrp docker
#Install git
sudo apt-get install git
```

Assign a ssh key to the node and associate with gitlab 
```
ssh-keygen -o -t rsa -b 4096 -C "<identifier>"
```

TODO (PRODUCTION): Setup transport certs (SERVER-SIDE)
stored in /etc/letsencrypt/live/lerring.co.uk/
```
apt-get install certbot
certbot certonly --standalone -d lerring.co.uk
chmod 0755 /etc/letsencrypt/{live,archive}
```

## Usage

setups folders required to run
```
sudo ./sh/0_initial_setup.sh {$NODE_TYPE}
```
Currently only does IP tables for the router
```
sudo ./sh/1_node_setup.sh {$NODE_TYPE}
```
Clones and builds relevant repositories
```
sudo ./sh/2_project_setup.sh {$NODE_TYPE}
```
Creates the service
```
sudo ./sh/3_service_setup.sh
```

```
sudo reboot
```

## Roadmap
The next steps of this project are to initialise a swarm using the following commands
```
docker swarm init
docker swarm join --token <token> 10.0.0.1:2377
```
